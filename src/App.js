import { useState } from "react";
import { NavLink, Outlet, useRoutes } from "react-router-dom";
import { Nav, Button, Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';


const routes = [{
  path: "/bflow",
  element: <BFlow />,
  children: [
    { path: "ctr", element: <Category cgr="ctr" num={0} /> },
    { path: "awr", element: <Category cgr="awr" num={0} /> },
  ],
  }, {
  path: "/oflow",
  element: <OFlow />,
  children: [
    { path: "slj", element: <Category cgr="slj" /> },
    { path: "fnc", element: <Category cgr="fnc" /> },
    { path: "est", element: <Category cgr="est" /> },
    { path: "cpr", element: <Category cgr="cpr" /> },
    { path: "awr", element: <Category cgr="awr" /> },
    { path: "quo", element: <Category cgr="quo" /> },
    { path: "bdm", element: <Category cgr="bdm" /> },
    { path: "bdf", element: <Category cgr="bdf" /> },
  ],
  }, {
  path: "/fflow",
  element: <FFlow />,
  children: [
    { path: "chr", element: <Category cgr="chr" /> },
    { path: "prj", element: <Category cgr="prj" /> },
  ],
  }, {
  path: "*",
  element: <Home />,
}];

function BFlow() {
  const sty = {minWidth: '30%', margin: '.3rem'}
  return (
    <>
      <header className="text-center">
        <NavLink to="ctr"><Button style={sty}>逆流</Button></NavLink>
        <NavLink to="awr"><Button style={sty}>覺察</Button></NavLink>
      </header>
      <main className="text-center">
        <Outlet />
      </main>
    </>
  );
}

function OFlow() {
  const sty = {minWidth: '20%', margin: '.3rem'}
  return (
    <>
      <header className="text-center">
       <Row className="mx-0">
       <Col>
        <NavLink to="slj"><Button style={sty}>副業</Button></NavLink>
        <NavLink to="fnc"><Button style={sty}>金融</Button></NavLink>
        <NavLink to="est"><Button style={sty}>地產</Button></NavLink>
        <NavLink to="cpr"><Button style={sty}>企業</Button></NavLink>
       </Col>
       </Row>
       <Row className="mx-0">
       <Col>
        <NavLink to="awr"><Button style={sty}>覺察</Button></NavLink>
        <NavLink to="quo"><Button style={sty}>行情</Button></NavLink>
        <NavLink to="bdm"><Button style={sty}>相親男</Button></NavLink>
        <NavLink to="bdf"><Button style={sty}>相親女</Button></NavLink>
       </Col>
       </Row>
      </header>
      <main className="text-center">
        <Outlet />
      </main>
    </>
  );
}

function FFlow() {
  const sty = {minWidth: '30%', margin: '.3rem'}
  return (
    <>
      <header className="text-center">
        <NavLink to="chr"><Button style={sty}>慈善</Button></NavLink>
        <NavLink to="prj"><Button style={sty}>項目</Button></NavLink>
      </header>
      <main className="text-center">
        <Outlet />
      </main>
    </>
  );
}

function Home() {
  return (
    <div>
      <p className="text-center">請從上方的圈層開始選擇牌卡種類</p>
    </div>
  );
}

function padLeadingZeros(num, size) {
  let s = num+"";
  while (s.length < size) s = "0"+s;
  return s;
}

function Category({ cgr }) {
  const [num, setNum] = useState(0)
  const cards = {
    'ctr': { name: '逆流', size: 54 },
    'slj': { name: '副業', size: 20 },
    'fnc': { name: '金融', size: 20 },
    'est': { name: '地產', size: 20 },
    'cpr': { name: '企業', size: 30 },
    'awr': { name: '覺察', size: 54 },
    'quo': { name: '行情', size: 26 },
    'bdm': { name: '相親男', size: 10 },
    'bdf': { name: '相親女', size: 10 },
    'chr': { name: '慈善', size: 17 },
    'prj': { name: '項目', size: 30 },
  }
  const sty = { maxWidth: '98%', maxHeight: '650px' }
  let card
  let padNum
  if (num === 0) {
    padNum = padLeadingZeros(num, 2)
    card = <img src={require(`./cards/${cgr}${padNum}.jpg`)} alt={cards[cgr].name} style={sty} onClick={() => {setNum(Math.floor(Math.random()*cards[cgr].size+1))}} />;
  } else {
    padNum = padLeadingZeros(num, 2)
    card = <img src={require(`./cards/${cgr}${padNum}.jpg`)} alt={cards[cgr].name+num} style={sty} onClick={() => {setNum(0)}} />;
  }
  return (
    <> {card} </>
  );
}

function App() {
  let element = useRoutes(routes);

  return (
    <div>
      <Nav fill variant="tabs" defaultActiveKey="/oflow" className="justify-content-center">
       <Nav.Item>
        <Nav.Link href="/bflow" eventKey="flow-1">逆流層</Nav.Link>
       </Nav.Item>
       <Nav.Item>
        <Nav.Link href="/oflow" eventKey="flow-2">平流層</Nav.Link>
       </Nav.Item>
       <Nav.Item>
        <Nav.Link href="/fflow" eventKey="flow-3">順流層</Nav.Link>
       </Nav.Item>
      </Nav>
      {element}
    </div>
  );
}

export default App;
